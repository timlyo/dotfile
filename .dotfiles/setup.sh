alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
git clone --bare git@gitlab.com:timlyo/dotfile.git $HOME/.dotfiles/ --work-tree=$HOME
dotfiles checkout
dotfiles config --local status.showUntrackedFiles no
