set number
set tabstop=4

call plug#begin('~/.vim/plugged')
		Plug 'rust-lang/rust.vim'
		Plug 'cespare/vim-toml'
		Plug 'ctrlpvim/ctrlp.vim'
call plug#end()

let g:ctrlp_user_command = ['.git', 'cd %s; git ls-files -co --exclude-standard']
