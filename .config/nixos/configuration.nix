{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
    ];

  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sda";
  boot.loader.grub.extraEntries = ''
    menuentry "Windoze" {
      chainloader (hd0,1)+1
    }
  '';

  boot.kernel.sysctl = {
	"vm.swappiness" = 10;
  };

  networking = {
	hostName = "nixos";
	networkmanager.enable = true;
  };

  time.timeZone = "Europe/London";

  environment.systemPackages = with pkgs; [
     #cmdline tools
     wget neovim git git-lfs htop zip unzip psmisc tree nox 
	 tldr nethogs alacritty termite watchexec libnotify ncdu
     # Window manager tools
     xorg.xkill arandr
     # Rust software 
     ripgrep exa alacritty fd rustc 
     # Apps
     firefox rofi vlc  zathura scrot xfce.thunar
     # Dev
     python36Full pipenv git-cola binutils
     # System
     acpi sysstat alsaUtils lm_sensors gksu udiskie
     ntfs3g networkmanagerapplet gparted usbutils pciutils
	 dunst networkmanager jdk
	 # Desktop
	 lxappearance nitrogen 
	 # Theme
	 arc-theme arc-icon-theme
	 # Dev
     gradle bazel
  ];

  programs.fish.enable = true;
  programs.java.enable = true;

  services.xserver.enable = true;
  services.xserver.layout = "gb";
  
  services.xserver.windowManager.i3 = {
	enable = true;
	extraPackages = with pkgs; [i3lock-fancy i3status];
  };

  services.compton = {
		enable = true;
		fade = true;
		vSync = "opengl";
  };

  virtualisation.docker.enable = true;

  services.xserver.libinput.enable = true;

  users.extraUsers.tim = {
    isNormalUser = true;
    extraGroups = ["wheel" "audio" "docker" "networkmanager"];
    shell = pkgs.fish;
    uid = 1000;
  };

  fonts = {
		fonts = with pkgs; [
				inconsolata font-awesome-ttf
		];
  };

  hardware.pulseaudio.enable = true;

  system.stateVersion = "17.09";
}
